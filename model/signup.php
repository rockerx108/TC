<?php

if (isset($_POST['submit']))
{
	include_once 'actionpage.php';

	$first = mysqli_real_escape_string($con, $_POST['fname']);
	$last = mysqli_real_escape_string($con, $_POST['lname']);
	$email = mysqli_real_escape_string($con, $_POST['email']);
	$uname = mysqli_real_escape_string($con, $_POST['uname']);
	$passwd = mysqli_real_escape_string($con, $_POST['passwd']);

	// Error Handlers
	// Check for empty fields

	If (empty($first) || empty($last) || empty($email) || empty($uname) || empty($passwd))
	{
		header("Location: ../signup.php?signup=empty");
		exit();
	}
	else
	{
		if (!preg_match("/^[a-zA-Z]*$/", $first) || (!preg_match("/^[a-zA-Z]*$/", $last)))
		{
			header("Location: ../signup.php?signup=invalid");
			exit();
		}
		else
		{

			// check if email is valid

			if (!filter_var($email, FILTER_VALIDATE_EMAIL))
			{
				header("Location: ../signup.php?signup=email");
				exit();
			}
			else
			{
				{
					$sql = "SELECT * FROM users WHERE user_uname='$uname'";
					$result = mysqli_query($con, $sql);
					$resultCheck = mysqli_num_rows($result);
				}
			}

			if ($resultCheck > 0)
			{
				header("Location: ../signup.php?signup=userTaken");
				exit();
			}
			else
			{

				// hashing the passwd

				$hashedpwd = password_hash($passwd, PASSWORD_DEFAULT);

				// insert the user into the databases

				$sql = "INSERT INTO users (user_first, user_last, user_email, user_uname, user_passwd) VALUES('$first','$last','$email','$uname','$hashedpwd');";
				mysqli_query($con, $sql);
				header("Location: ../signup.php?signup=userTaken");
				exit();
			}
		}
	}
}