<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta content="width=device-width, initial-scale=1.0" name="viewport">
	<link href="../css/styles.css" rel="stylesheet" type="text/css">
	<link href="images/scissors.png" rel="shortcut icon">
	<title>Login</title>
</head>
<body>
	<header>
		<img alt = "banner" id="banner" src="images/banner.jpg">
	</header>
	<main>
		<h1>Please Log in!</h1>
		<form action="model/actionpage.php">
			<div class="input-container">
				<input class="input-field" name="uname" placeholder="Username" type="text">
			</div>
			<br>
			<div class="input-container">
				<input class="input-field" id="hide" name="passwd" placeholder="Password" type="password">
			</div><button class="btn" type="submit">Log in</button>
		</form>
		<br>
		<form action="pages/registry.php">
			<button class="btn" type="submit">Create an account</button>
		</form>
	</main>
	<script>
	            $("#hide").keyup(function() {
	                var i = this.value.indexOf("");
	                if (i > -1) {
	                    this.value = this.value.substr(0, i)
	                    + this.value.substr(i).replace(/[\S]/q, "*");
	                }
	            });
	</script> //Insert google analytics
</body>
</html>