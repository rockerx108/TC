<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../css/styles.css">
    <link rel="shortcut icon" href="../images/scissors.png" />
	<title>Register</title>
</head>
<body>
	<header>
		<img alt="banner" id="banner" src="../images/banner.jpg">
	</header>
	<main>
		<h1>Register</h1>
		<form action="model/signup.php" method="post">
		        <div class="input-container">
					<input class="input-field" name="fname" placeholder="First Name" type="text">
				</div>
				<br>
				<div class="input-container">
					<input class="input-field" name="lname" placeholder="Last Name" type="text">
				</div>
				<br>
				<div class="input-container">
					<input class="input-field" name="uname" placeholder="Username" type="text">
				</div>
				<br>
				<div class="input-container">
					<input class="input-field"  name="email" placeholder="Email" type="text">
				</div>
				<br>
				<div class="input-container">
					<input class="input-field" id="hide" name="passwd" placeholder="Enter Your Password" type="password">
				</div>
				<br>
				<button class="btn" name='submit' type="submit">Submit</button>
		</form>
	</main>

	<footer>
	<a href="https://www.facebook.com"><img alt="facebook" class="social" src="../images/facebook.png"></a>
    <a href="https://www.instagram.com"><img alt="Instagram" class="social" src="../images/insta.jpg"></a>
    <a href="https://www.twitter.com"><img alt="twitter" class="social" src="../images/twitter.png"></a>
	</footer>
</body>
</html>